﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScreenShotDemo;
using WPFFolderBrowser;

namespace Machine_Learning_Trainer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.FixedSingle;
            ControlBox = false;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            int o;
            TopMost = true;
            RetryAsk:
            WPFFolderBrowserDialog d = new WPFFolderBrowserDialog("Select the folder to dump the images");
            d.ShowDialog();
            if(d.FileName != "" || d.FileName != null)
            {

            }
            else
            {
                goto RetryAsk;
            }

            for (int i = 3; i >= 0; i--)
            {
                Text = "Will start in " + i + " ..";
                await Task.Delay(1000);
            }
            Visible = false;
            if(Int32.TryParse(textBox1.Text,out o))
            {
                BackgroundWorker Screenshotter = new BackgroundWorker();
                bool Stop = false;
                bool ActuallyStopped = false;
                Screenshotter.RunWorkerCompleted += (sender1, args) =>
                {
                    ActuallyStopped = true;
                };
                Screenshotter.DoWork += async (sender1, args) =>
                {
                    ScreenCapture Capture = new ScreenCapture();
                    while(!Stop)
                    {
                        RegenerateFileName:
                        string RandomFileName = d.FileName + "\\" + new Random().Next(11111, 99999) + ".png";
                        if(File.Exists(RandomFileName)) goto RegenerateFileName;
                        Capture.CaptureScreenToFile(RandomFileName,ImageFormat.Png);
                        await Task.Delay(500);
                    }
                };
                Screenshotter.RunWorkerAsync();
                await Task.Delay(TimeSpan.FromMinutes(o));
                Stop = true;
                while(!ActuallyStopped) await Task.Delay(10);
                if(Directory.Exists(d.FileName)) Process.Start(d.FileName);
            }
            TopMost = false;
            Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
