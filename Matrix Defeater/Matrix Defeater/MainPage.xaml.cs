﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Renci.SshNet;
using Renci.SshNet.Common;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Matrix_Defeater
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Loaded += MainPage_Loaded;
        }

        private class MainFiles
        {
            public static string Technology = ApplicationData.Current.LocalFolder.Path + "\\Matrix\\Technology";
        }

        CommonHelpers Helper = new CommonHelpers();
        private async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            //StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appdata:///local/file.txt"));
            MainGrid.Visibility = Visibility.Collapsed;
            if (File.Exists(MainDirectory + "\\User"))
            {
                UsernameTextBox.Text = File.ReadAllText(MainDirectory + "\\User");
            }
            if(File.Exists(MainDirectory + "\\Pass"))
            {
                PasswordTextBox.Password = File.ReadAllText(MainDirectory + "\\Pass");
                LoginMatrix();
            }
            Directory.CreateDirectory(MainDirectory);
        }

        ConnectionInfo MatrixConnection;
        SshClient client;

        string MainDirectory = ApplicationData.Current.LocalFolder.Path + "\\Matrix";
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            LoginMatrix();
        }

        private void LoginMatrix()
        {
            bool SocketError = false, WrongPassword = false;
            MatrixConnection = new ConnectionInfo("matrix.senecacollege.ca", UsernameTextBox.Text.Replace("@myseneca.ca", ""), new PasswordAuthenticationMethod(UsernameTextBox.Text.Replace("@myseneca.ca", ""), PasswordTextBox.Password));
            MatrixConnection.AuthenticationBanner += (o, args) =>
            {
                Console.WriteLine(args.BannerMessage);
            };
            client = new SshClient(MatrixConnection);
            try
            {
                client.Connect();
            }
            catch (SocketException)
            {
                SocketError = true;
                MessageDialog d = new MessageDialog("Please connect to Seneca VPN");
                d.ShowAsync();
            }
            catch (SshAuthenticationException)
            {
                WrongPassword = true;
                MessageDialog d = new MessageDialog("Wrong username or password");
                d.ShowAsync();
            }
            if (MatrixConnection.IsAuthenticated)
            {
                LoginGrid.Visibility = Visibility.Collapsed;
                MainGrid.Visibility = Visibility.Visible;
                if (RememberMeCheckBox.IsChecked == true)
                {
                    File.WriteAllText(MainDirectory + "\\User", UsernameTextBox.Text);
                    File.WriteAllText(MainDirectory + "\\Pass", PasswordTextBox.Password);
                }
                if (!File.Exists(MainFiles.Technology))
                {
                    File.WriteAllText(MainFiles.Technology, "TouchlessMatrix");
                }
                RunLoggedInCommands();
                shell = client.CreateShellStream("Main",80,40,80,40,1024);
            }
        }

        ShellStream shell;

        private void TechnologyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            File.WriteAllText(MainFiles.Technology,TechnologyComboBox.Text);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            client.Disconnect();
            MainGrid.Visibility = Visibility.Collapsed;
            LoginGrid.Visibility = Visibility.Visible;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private async void SubmitAssignmentButton_Click(object sender, RoutedEventArgs e)
        {
            if (AssignmentComboBox.SelectedIndex == 0)
            {
                string RawInput = new WebClient().DownloadString("http://matrix.bigheados.com/inputs/a1ms4.txt");
                foreach (string s in RawInput.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                {
                    Helper.MatrixInputs.Add(s);
                }
                ChangeDirectory(AssignmentDirectoryTextBox.Text);
                await Task.Delay(10);
                client.RunCommand("~ian.tipson/submit 144a1ms1/NEE_ms1");
                foreach (string s in Helper.MatrixInputs)
                {
                    shell.WriteLine(s);
                }

                shell.WriteLine("y");
            }
        }

        private void ChangeDirectory(string directory)
        {
            string cmdStr = "cd " + directory + "; ls -la";
            client.RunCommand(cmdStr);
            UserInfoLabel.Text = "Current Directory: " + client.RunCommand("pwd").Execute();
        }
    }
}
