﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Matrix_Defeater
{
    public sealed partial class MainPage : Page
    {
        private void RunLoggedInCommands()
        {
            string WhoamI = client.RunCommand("whoami").Result;
            string pwd = client.RunCommand("pwd").Result;
            UserInfoLabel.Text = "Username: " + WhoamI + "\nCurrent Directory: " + pwd;
        }
    }
}
