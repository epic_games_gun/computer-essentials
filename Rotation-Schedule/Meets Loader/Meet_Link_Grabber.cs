﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Meets_Loader
{
    public partial class Meet_Link_Grabber : Form
    {
        public Meet_Link_Grabber()
        {
            InitializeComponent();
        }

        private string HTML()
        {
            string HTMLd = webView1.GetHtml();
            return HTMLd;
        }

        public static string JoinLink = "";
        private async void Meet_Link_Grabber_Load(object sender, EventArgs e)
        {
            webView1.Url = Form1.ClassroomLink;
            while (webView1.IsLoading || !HTML().Contains("meet.google.com"))
            {
                await Task.Delay(10);
            }
            string HTMLs = HTML();
            string GrabLink = "";
            int LineCount = 0;
            foreach (string S in HTMLs.Split(new []{Environment.NewLine},StringSplitOptions.None))
            {
                if (S.Contains("meet.google.com"))
                {
                    GrabLink = S;
                    break;
                }
                LineCount++;
            }

            foreach (string S in GrabLink.Split('"'))
            {
                if(S.Contains("meet.google.com"))
                {
                    GrabLink = S;
                }
            }
            GrabLink = GrabLink.Replace(">", "").Replace("<svg", "").Replace("enable-background=", "");
            Console.WriteLine(GrabLink);
            Form1.ClassroomLink = GrabLink;
            Form1.MeetLink = GrabLink;
            JoinLink = GrabLink;
            Close();
        }
    }
}
