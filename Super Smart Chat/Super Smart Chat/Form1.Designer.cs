﻿namespace Super_Smart_Chat
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ResponseBox = new System.Windows.Forms.RichTextBox();
            this.StartButton = new System.Windows.Forms.Button();
            this.AutoRunCheckBox = new System.Windows.Forms.CheckBox();
            this.EnableRichText = new System.Windows.Forms.Button();
            this.DisableRichText = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CounterLabel = new System.Windows.Forms.Label();
            this.CopyTextButton = new System.Windows.Forms.Button();
            this.QuestionBox1 = new System.Windows.Forms.RichTextBox();
            this.QuestionBox2 = new System.Windows.Forms.RichTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.AutoURL = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ResponseLength = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TempuratureTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.UploadImageButton = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ResponseBox
            // 
            this.ResponseBox.Location = new System.Drawing.Point(18, 98);
            this.ResponseBox.Name = "ResponseBox";
            this.ResponseBox.ReadOnly = true;
            this.ResponseBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ResponseBox.Size = new System.Drawing.Size(670, 318);
            this.ResponseBox.TabIndex = 0;
            this.ResponseBox.Text = "";
            // 
            // StartButton
            // 
            this.StartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.StartButton.Location = new System.Drawing.Point(710, 18);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(273, 75);
            this.StartButton.TabIndex = 3;
            this.StartButton.Text = "Start";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // AutoRunCheckBox
            // 
            this.AutoRunCheckBox.AutoSize = true;
            this.AutoRunCheckBox.Checked = true;
            this.AutoRunCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AutoRunCheckBox.Location = new System.Drawing.Point(710, 200);
            this.AutoRunCheckBox.Name = "AutoRunCheckBox";
            this.AutoRunCheckBox.Size = new System.Drawing.Size(202, 17);
            this.AutoRunCheckBox.TabIndex = 4;
            this.AutoRunCheckBox.Text = "Auto Run if no updates for 2 seconds";
            this.AutoRunCheckBox.UseVisualStyleBackColor = true;
            // 
            // EnableRichText
            // 
            this.EnableRichText.Location = new System.Drawing.Point(710, 236);
            this.EnableRichText.Name = "EnableRichText";
            this.EnableRichText.Size = new System.Drawing.Size(100, 34);
            this.EnableRichText.TabIndex = 5;
            this.EnableRichText.Text = "Enable";
            this.EnableRichText.UseVisualStyleBackColor = true;
            this.EnableRichText.Click += new System.EventHandler(this.EnableRichText_Click);
            // 
            // DisableRichText
            // 
            this.DisableRichText.Location = new System.Drawing.Point(882, 236);
            this.DisableRichText.Name = "DisableRichText";
            this.DisableRichText.Size = new System.Drawing.Size(100, 34);
            this.DisableRichText.TabIndex = 6;
            this.DisableRichText.Text = "Disable";
            this.DisableRichText.UseVisualStyleBackColor = true;
            this.DisableRichText.Click += new System.EventHandler(this.DisableRichText_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(814, 239);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 26);
            this.label1.TabIndex = 7;
            this.label1.Text = "Edit Text\r\n<=======>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Enter your question below, smarter than Siri";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 425);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(350, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Enter your question below if your cursor is closer to here, smarter than Siri";
            // 
            // CounterLabel
            // 
            this.CounterLabel.AutoSize = true;
            this.CounterLabel.Location = new System.Drawing.Point(707, 220);
            this.CounterLabel.Name = "CounterLabel";
            this.CounterLabel.Size = new System.Drawing.Size(35, 13);
            this.CounterLabel.TabIndex = 10;
            this.CounterLabel.Text = "label4";
            // 
            // CopyTextButton
            // 
            this.CopyTextButton.Location = new System.Drawing.Point(710, 274);
            this.CopyTextButton.Name = "CopyTextButton";
            this.CopyTextButton.Size = new System.Drawing.Size(272, 45);
            this.CopyTextButton.TabIndex = 11;
            this.CopyTextButton.Text = "Copy Text";
            this.CopyTextButton.UseVisualStyleBackColor = true;
            this.CopyTextButton.Click += new System.EventHandler(this.CopyTextButton_Click);
            // 
            // QuestionBox1
            // 
            this.QuestionBox1.Location = new System.Drawing.Point(18, 32);
            this.QuestionBox1.Name = "QuestionBox1";
            this.QuestionBox1.Size = new System.Drawing.Size(673, 61);
            this.QuestionBox1.TabIndex = 12;
            this.QuestionBox1.Text = "";
            this.QuestionBox1.TextChanged += new System.EventHandler(this.QuestionBox1_TextChanged_1);
            // 
            // QuestionBox2
            // 
            this.QuestionBox2.Location = new System.Drawing.Point(18, 441);
            this.QuestionBox2.Name = "QuestionBox2";
            this.QuestionBox2.Size = new System.Drawing.Size(673, 61);
            this.QuestionBox2.TabIndex = 13;
            this.QuestionBox2.Text = "";
            this.QuestionBox2.TextChanged += new System.EventHandler(this.QuestionBox2_TextChanged_1);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(715, 322);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(266, 179);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.TempuratureTextBox);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.ResponseLength);
            this.tabPage1.Controls.Add(this.AutoURL);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(258, 153);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // AutoURL
            // 
            this.AutoURL.AutoSize = true;
            this.AutoURL.Checked = true;
            this.AutoURL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AutoURL.Location = new System.Drawing.Point(3, 6);
            this.AutoURL.Name = "AutoURL";
            this.AutoURL.Size = new System.Drawing.Size(107, 17);
            this.AutoURL.TabIndex = 0;
            this.AutoURL.Text = "Auto Open URLs";
            this.AutoURL.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(258, 153);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ResponseLength
            // 
            this.ResponseLength.Location = new System.Drawing.Point(6, 42);
            this.ResponseLength.Name = "ResponseLength";
            this.ResponseLength.Size = new System.Drawing.Size(100, 20);
            this.ResponseLength.TabIndex = 1;
            this.ResponseLength.Text = "2000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Response length";
            // 
            // TempuratureTextBox
            // 
            this.TempuratureTextBox.Location = new System.Drawing.Point(125, 42);
            this.TempuratureTextBox.Name = "TempuratureTextBox";
            this.TempuratureTextBox.Size = new System.Drawing.Size(43, 20);
            this.TempuratureTextBox.TabIndex = 3;
            this.TempuratureTextBox.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(122, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tempurature";
            // 
            // UploadImageButton
            // 
            this.UploadImageButton.Location = new System.Drawing.Point(708, 112);
            this.UploadImageButton.Name = "UploadImageButton";
            this.UploadImageButton.Size = new System.Drawing.Size(116, 49);
            this.UploadImageButton.TabIndex = 15;
            this.UploadImageButton.Text = "Upload Image";
            this.UploadImageButton.UseVisualStyleBackColor = true;
            this.UploadImageButton.Click += new System.EventHandler(this.UploadImageButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 509);
            this.Controls.Add(this.UploadImageButton);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.QuestionBox2);
            this.Controls.Add(this.QuestionBox1);
            this.Controls.Add(this.CopyTextButton);
            this.Controls.Add(this.CounterLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DisableRichText);
            this.Controls.Add(this.EnableRichText);
            this.Controls.Add(this.AutoRunCheckBox);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.ResponseBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Super Smart Chat";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox ResponseBox;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.CheckBox AutoRunCheckBox;
        private System.Windows.Forms.Button EnableRichText;
        private System.Windows.Forms.Button DisableRichText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label CounterLabel;
        private System.Windows.Forms.Button CopyTextButton;
        private System.Windows.Forms.RichTextBox QuestionBox1;
        private System.Windows.Forms.RichTextBox QuestionBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox AutoURL;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox ResponseLength;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TempuratureTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button UploadImageButton;
    }
}

