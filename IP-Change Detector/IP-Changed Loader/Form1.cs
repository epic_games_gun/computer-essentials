﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IP_Changed_Loader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            string Filee = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\IPChange.exe";
            if (!File.Exists(Filee))
            {
                File.Copy(Application.ExecutablePath,Filee);
            }
            await new WebClient().DownloadFileTaskAsync(new Uri("https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/IP-Change%20Detector/IP-Change%20Detector/bin/Debug/IP-Change%20Detector.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\IP.exe");
            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\IP.exe");
            Application.Exit();
        }
    }
}
