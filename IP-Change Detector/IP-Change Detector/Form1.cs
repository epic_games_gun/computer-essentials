﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IPAPIName;
using UsefulTools;

namespace IP_Change_Detector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string PingWad = "<@557317197345849344>";
        string DiscordLink = "https://discordapp.com/api/webhooks/864031720037416960/u7-CyMoR_z8QfUlaURtdE8i-vXLArh71kPjYTIDw6yClehk_YeNSostG35OgrEiJRqLI";
        bool WadPingDisable = false;
        private async Task SendDiscordWebHook(string Message, string BotLink)
        {
            try
            {
                if (WadPingDisable) Message = Message.Replace(PingWad, "");
                using (var client = new WebClient())
                {
                    NameValueCollection name = new NameValueCollection();
                    name.Add("username", "DoorDash Info");
                    name.Add("content", Message);
                    client.UploadValues(BotLink, name);
                }
            }
            catch (Exception)
            {

            }
            await Task.Delay(100);
        }
        private async void Form1_Load(object sender, EventArgs e)
        {
            string OldIP = new WebClient().DownloadString("http://icanhazip.com");
            string NewIP = OldIP;
            string IPInfoo = new WebClient().DownloadString("http://ip-api.com/json/" + OldIP.Replace("\n", "") + "?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,asname,reverse,mobile,proxy,hosting,query");
            while (true)
            {
                try
                {
                    Visible = false;
                    await Task.Delay(10);
                    NewIP = new WebClient().DownloadString("http://icanhazip.com");
                    await Internet.CheckInternet();
                    if (OldIP != NewIP &&!CheckForVPNInterface())
                    {
                        Console.WriteLine("IP CHANGED");
                        await Internet.CheckInternet();
                        Console.WriteLine("Restored Internet");
                        OldIP = NewIP;
                        var IPInfo = Ipapi.FromJson(new WebClient().DownloadString("http://ip-api.com/json/" + OldIP.Replace("\n","") + "?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,asname,reverse,mobile,proxy,hosting,query"));
                        string IPStatus = NewIP.Contains("209") ? "CIK IP\n" : "Connected to VPN\n";
                        bool DoorDashable = !IPInfo.Proxy && !IPInfo.Hosting;
                        string MoreInfo = "\n\n\n\n```\n" + "ISP: " + IPInfo.Isp + "\n" + "Country: " + IPInfo.Country + "\n" + "City: " + IPInfo.City + "\n" + "Region: " + IPInfo.Region + "\n" + "\nLongitude: " + IPInfo.Lon + ", Latitude: " + IPInfo.Lat + "\nVPN: " + IPInfo.Proxy + "\n" + "DoorDashAble: " + DoorDashable + "\n```";
                        await SendDiscordWebHook("```\nIP Address has been changed\nIP:" + OldIP + "\n" + IPStatus + "\nDateTime: " + DateTime.Now.ToString("F") + "\n```\n" + PingWad + MoreInfo, DiscordLink);
                    }
                }
                catch (Exception)
                {
                    
                }
            }
        }

        public bool CheckForVPNInterface()
        {
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                NetworkInterface[] interfaces =
                    NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface Interface in interfaces)
                {
                    // This is the OpenVPN driver for windows. 
                    if (Interface.Description.Contains("TAP-Nord")
                        && Interface.OperationalStatus == OperationalStatus.Up)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
