﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Media;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Coronavirus_API;
using Covid19API;
using MailKit;
using MailKit.Net.Imap;
using Main_Tools_Remastered.Properties;
using MaterialDesignThemes.Wpf;
using QuickType;
using UsefulTools;
using BooleanToVisibilityConverter = MaterialDesignThemes.Wpf.Converters.BooleanToVisibilityConverter;

namespace Main_Tools_Remastered
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public class AbortableBackgroundWorker : BackgroundWorker
    {

        private Thread workerThread;

        protected override void OnDoWork(DoWorkEventArgs e)
        {
            workerThread = Thread.CurrentThread;
            try
            {
                base.OnDoWork(e);
            }
            catch (ThreadAbortException)
            {
                e.Cancel = true; //We must set Cancel property to true!
                Thread.ResetAbort(); //Prevents ThreadAbortException propagation
            }
        }


        public void Abort()
        {
            if (workerThread != null)
            {
                workerThread.Abort();
                workerThread = null;
            }
        }
    }
    public partial class MainWindow : Window
    {
        ImapClient email = new ImapClient();
        bool LoggedIn = false;
        bool EmailDisconnected = false;
        string OTPPath = Environment.GetEnvironmentVariable("APPDATA") + "\\OTP";
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            Deactivated += MainWindow_Deactivated;
            Closing += MainWindow_Closing;
            CloseButton.Click += CloseButton_Click;
            if (!LoggedIn && Directory.Exists(OTPPath))
            {
               
                LoggedIn = true;
            }
        }

        private void MainWindow_Deactivated(object sender, EventArgs e)
        {
            // The Window was deactivated 
            Topmost = false; // set topmost false first
            Topmost = true; // then set topmost true again
        }

        private async void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            await ClosePopup();
        }

        MainFunctions Functions = new MainFunctions();
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }
        bool OwnsNetGear = false;

        Covid19 Covid;
        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ShowInTaskbar = false;
            Visibility = Visibility.Hidden;
            SpeedTestGrid.Visibility = Visibility.Hidden;
            Topmost = true;
            while (await Functions.CheckInternet() == false)
            {
                await Task.Delay(10);
            }
            string OldCoronaCases = await Coronavirus.GetCoronavirusCases();
            string OldCoronaDeaths = await Coronavirus.GetDeathCoronavirusCases();
            string OldCoronaVaccines = await Coronavirus.GetVaccineCoronavirusCases();
            Console.WriteLine("Coronavirus cases: " + OldCoronaCases + " , Deaths: " + OldCoronaDeaths + " , Vaccines: " + OldCoronaVaccines);
            string OldIP = await Functions.CheckIP();
            Console.WriteLine("Current IP: " + OldIP);
            if (await Functions.CheckNetGear() == true)
            {
                Console.WriteLine("Asus router detected");
                OtherFunctions.ShowNotification("Asus Router", "Asus router has been detected");
                NetGearCheckBox.IsChecked = true;
                OwnsNetGear = true;
                await Functions.PlaySoundSync(Properties.Resources.AsusRouter);
            }

            if (Directory.Exists(OTPPath))
            {
                string Username = File.ReadAllText(OTPPath + "\\User.txt");
                string Password = File.ReadAllText(OTPPath + "\\Password.txt");
                email.Disconnected += async (d, args) =>
                {
                    Console.WriteLine("Client disconnected, should fire now");
                    await PopupSound("Internet may not be available", "Your internet was disconnected/lost connection with CIK\nChecked by Version 2", Properties.Resources.InternetMayNotBeAvailable);
                    EmailDisconnected = true;
                    await Functions.PureCheckInternet();
                    await email.ConnectAsync("imap-mail.outlook.com", 993, true);
                    await email.AuthenticateAsync(Username, Password);
                    await email.Inbox.OpenAsync(FolderAccess.ReadWrite);
                };
                email.Connected += async (d, args) =>
                {
                    Console.WriteLine("Client restored");
                    EmailDisconnected = false;
                };
                Console.WriteLine("Contains OTP Path");
                
                await email.ConnectAsync("imap-mail.outlook.com", 993, true);
                await email.AuthenticateAsync(Username, Password);
                await email.Inbox.OpenAsync(FolderAccess.ReadWrite);
            }
            Console.WriteLine("--PASSED BORDER--\nProgram now starting");
            Stopwatch CoronaElapse = new Stopwatch();
            CoronaElapse.Start();
            //OldWeather = "";
            while (true)
            {
                try
                {
                    await Functions.HoldFile();
                    while (File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\HoldMonitoring.txt"))
                    {
                        await Task.Delay(10);
                    }
                    await Task.Delay(100);
                    if (OwnsNetGear == false)
                    {
                        if (await Functions.CheckInternet() == false)
                        {
                            PassError = "Rogersed, blame ISP or your internetn't";
                            OtherFunctions.ShowNotification("CIK'ed", "Your internet may not be available");
                            await PopupSound("Internet may not be available",
                                "Your internet might not be working\nCheck router settings, windows settings\nOr you may have configured wrong wifi settings.",
                                Properties.Resources.InternetMayNotBeAvailable);
                            await DoSpeedTest(false);
                        }
                    }
                    else
                    {
                        if (await Functions.CheckInternet() == false && await Functions.CheckNetGear() == true)
                        {
                            PassError = "CIK'ed, blame ISP or your internetn't";
                            OtherFunctions.ShowNotification("CIK'ed", "Your internet may not be available");
                            await PopupSound("Internet may not be available",
                                "Your internet might not be working\nCheck router settings, windows settings\nOr you may have configured wrong wifi settings.",
                                Properties.Resources.InternetMayNotBeAvailable);
                            await DoSpeedTest(false);
                        }
                        else if (await Functions.CheckInternet() == false && await Functions.CheckNetGear() == false)
                        {
                            PassError = "Router is netgeared";
                            OtherFunctions.ShowNotification("ROUTER ASUS'ED",
                                "Please restart ASUS router, it will only take 30 seconds");
                            await NetGearedPopup();
                        }
                    }

                    Covid = Covid19.FromJson(
                        new WebClient().DownloadString("http://covid.bigheados.com/worldwide/cases.txt"));
                    string NewCoronaCases = Covid.ConfirmedCases.Replace(",", "");
                    Console.WriteLine("Current COVID-19 Cases WorldWide: " + NewCoronaCases + ", Old cases: " +
                                      OldCoronaCases);
                    if (Int32.Parse(OldCoronaCases) + 10000 < Int32.Parse(NewCoronaCases))
                    {
                        await OtherFunctions.ShowNotification("COVID-19 Cases Reported",
                            "More than 10000 cases of COVID-19 has been reported!");
                        await Functions.PlaySoundSync(Properties.Resources.Alarm);
                        int Difference = Int32.Parse(NewCoronaCases) - Int32.Parse(OldCoronaCases);
                        int OneHundredMillion = 200000000;
                        var EstimatedDays = new DateTime();
                        EstimatedDays = DateTime.Now;
                        File.WriteAllText(
                            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                            "\\CovidCaseDifference.txt", Difference.ToString("0"));
                        int CoronaElapseMinutes = CoronaElapse.Elapsed.Minutes;
                        int CoronaElapseHours = CoronaElapse.Elapsed.Hours;
                        CoronaElapse.Stop();
                        CoronaElapse.Reset();
                        File.WriteAllText(
                            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                            "\\CovidCaseElapsedTime.txt", CoronaElapseMinutes.ToString());
                        int RemainingCases = OneHundredMillion - Int32.Parse(NewCoronaCases);
                        double HowManyTimesBeforeThat = RemainingCases / Difference;
                        double TheMinutesBeforeThat = HowManyTimesBeforeThat * CoronaElapseMinutes;
                        EstimatedDays = EstimatedDays.AddMinutes(TheMinutesBeforeThat);
                        Console.WriteLine(EstimatedDays.ToString("g"));
                        await PopupSound("Coronavirus cases reported",
                            "More than 10000 cases of COVID-19 cases was reported\nOld cases: " + OldCoronaCases +
                            "\nNew cases: " + NewCoronaCases + "\nDifference: " + Difference +
                            "\n\nEstimated Date When It Is Two Hundred Million Cases: " + EstimatedDays.ToString("g") +
                            "\nElapsed: " + CoronaElapseHours + " hours and " + CoronaElapseMinutes + " minutes",
                            Properties.Resources.Stylish_10Thousand);
                        OldCoronaCases = NewCoronaCases;
                        string NewCoronaDeaths = Covid.Deaths.Replace(",", "");
                        CoronaElapse.Start();
                        if (Int32.Parse(OldCoronaDeaths) + 1000 < Int32.Parse(NewCoronaDeaths))
                        {
                            OtherFunctions.ShowNotification("COVID-19 Cases Reported",
                                "More than 1000 cases of COVID-19 deaths has been reported!");
                            await Functions.PlaySoundSync(Properties.Resources.CodeBlack);
                            await PopupSound("Coronavirus deaths reported",
                                "More than 1000 cases of COVID-19 deaths have been reported.\nOld deaths: " +
                                OldCoronaDeaths + "\nNew deaths: " + NewCoronaDeaths + "\nDifference: " +
                                (Int32.Parse(NewCoronaDeaths) - Int32.Parse(OldCoronaDeaths)).ToString("0"),
                                Properties.Resources.Stylish_1000Thousand);
                            OldCoronaDeaths = NewCoronaDeaths;
                        }
                    }

                    string NewIP = await Functions.CheckIP();
                    if (OldIP != NewIP && NewIP != "")
                    {
                        await PopupSound("Public IP address changed",
                            "Your public IP address has change, due to connection to other network and/or VPN\n\nOld IP: " +
                            OldIP + "\n\nNew IP: " + NewIP, Properties.Resources.NewIPChanged);
                        OldIP = NewIP;
                        await DoSpeedTest(false);
                    }


                    // 30 Seconds cooldown //
                    Console.WriteLine("Waiting for 30 seconds cooldown");
                    for (int i = 0; i < 1; i++)
                    {
                        await Task.Delay(500);
                        if (await Functions.CheckInternet() == false ||
                            OwnsNetGear == true && await Functions.CheckNetGear() == false)
                        {
                            break;
                        }
                    }

                    if (!email.IsConnected && Directory.Exists(OTPPath))
                    {
                        await PopupSound("Internet may not be available", "ROGERSED, or program error, program cannot connect to internet.\n\nClient Cannot Connect", Properties.Resources.InternetMayNotBeAvailable);
                        await DoSpeedTest(false);
                    }
                    Console.WriteLine("Countdown passed");
                }
                catch (WebException)
                {
                    Console.WriteLine("A minor exception");
                }
                catch (Exception ex)
                {
                    PassError = ex.ToString();
                    await PopupSound("Internet may not be available", "ROGERSED, or program error, program cannot connect to internet.\n\n" + ex.ToString(), Properties.Resources.InternetMayNotBeAvailable);
                    await DoSpeedTest(false);
                    Console.WriteLine(ex);
                }
            }
        }
        bool ClosedPopup = false;

        
        bool GamingMode = false;
        
        

        private void SpeedTestCloseButton_Click(object sender, RoutedEventArgs e)
        {
            SpeedTestGrid.Visibility = Visibility.Hidden;
            MainPopupGrid.Visibility = Visibility.Visible;
            Visibility = Visibility.Hidden;
        }

        string SpeedTestURL = "";
        private void SpeedTestResultButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(SpeedTestURL.Replace(" ", ""));
            }
            catch 
            {

            }
        }

       
    }
}
