﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Main_Tools_Remastered.Properties;
using MaterialDesignThemes.Wpf;
using UsefulTools;

namespace Main_Tools_Remastered
{
    public partial class MainWindow : Window
    {
        SnackbarMessageQueue Queue = new SnackbarMessageQueue(TimeSpan.FromSeconds(3));

        string WeatherSoundDirectory = Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\WeatherSounds";
        private async Task PlayWeatherSound(WeatherSound Sound)
        {
            TimeSpan start = new TimeSpan(8, 0, 0); //10 o'clock
            TimeSpan end = new TimeSpan(23, 0, 0); //12 o'clock
            TimeSpan now = DateTime.Now.TimeOfDay;

            if ((now > start) && (now < end))
            {
                if (!Directory.Exists(WeatherSoundDirectory))
                {
                    Directory.CreateDirectory(WeatherSoundDirectory);
                    using (var client = new WebClient())
                    {
                        client.DownloadFileAsync(
                            new Uri(
                                "https://raw.githubusercontent.com/EpicGamesGun/Monitoring-Tools/master/Bixby%20Alarm.zip"),
                            Environment.GetEnvironmentVariable("TEMP") + "\\WeatherSounds.Zip");
                        while (client.IsBusy)
                        {
                            await Task.Delay(10);
                        }
                    }

                    ZipFile.ExtractToDirectory(Environment.GetEnvironmentVariable("TEMP") + "\\WeatherSounds.Zip",
                        WeatherSoundDirectory);
                }
                if (Sound == WeatherSound.Breezy)
                {
                    await PlaySoundFromWeatherDirectory("default_sound.wav");
                }
                else if (Sound == WeatherSound.FogCloudy)
                {
                    await PlaySoundFromWeatherDirectory("fog_cloudy.wav");
                }
                else if (Sound == WeatherSound.Rain)
                {
                    await PlaySoundFromWeatherDirectory("rain.wav");
                }
                else if (Sound == WeatherSound.Snow)
                {
                    await PlaySoundFromWeatherDirectory("snow.wav");
                }
                else if (Sound == WeatherSound.Sunny)
                {
                    await PlaySoundFromWeatherDirectory("sunny_clear_hot_warm.wav");
                }
                else if (Sound == WeatherSound.Thunderstorm)
                {
                    await PlaySoundFromWeatherDirectory("thunder.wav");
                }
                else if (Sound == WeatherSound.Windy)
                {
                    await PlaySoundFromWeatherDirectory("windy_cold.wav");
                }
            }
        }

        private async Task PlaySound(Stream location)
        {
            SoundPlayer d = new SoundPlayer(location);
            d.Play();
        }

        enum WeatherSound
        {
            Breezy, FogCloudy, Rain, Snow, Sunny, Thunderstorm, Windy
        }

        private async Task PlaySoundFromWeatherDirectory(string Sound)
        {
            if (File.Exists(WeatherSoundDirectory + "\\" + Sound))
            {
                string SoundD = WeatherSoundDirectory + "\\" + Sound;
                await Task.Factory.StartNew(() =>
                {
                    new SoundPlayer(SoundD).PlaySync();
                });
            }
        }
        private async Task ShowSnackBar(string Message)
        {
            UniversalSnackbar.MessageQueue = Queue;
            Queue.Enqueue(Message);
        }

        private async Task ShowSnackBar(string Message, string ButtonContent, Action Button)
        {
            UniversalSnackbar.MessageQueue = Queue;
            Queue.Enqueue(Message, ButtonContent, Button);
        }
        private async Task PopupSound(string ErrorTitle, string ReasonMessage, Stream Sound)
        {
            await Functions.HoldFile();
            if (GamingModeCheckBox.IsChecked == true)
            {
                GamingMode = true;
            }
            else
            {
                GamingMode = false;
            }
            SpeedTestGrid.Visibility = Visibility.Hidden;
            MainPopupGrid.Visibility = Visibility.Visible;
            AbortCountDown = false;
            ClosedPopup = false;
            SoundPlayer Player = new SoundPlayer(Sound);
            ErrorLabel.Content = ErrorTitle;
            ReasonLabel.Content = ReasonMessage;
            Topmost = true;
            Player.Play();
            if (GamingMode == false)
            {
                Visibility = Visibility.Visible;
                Topmost = true;
                await AutoClosePopup(30);
            }
            else
            {
                await AutoClosePopup(10);
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
                {
                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
                    GamingMode = false;
                    GamingModeCheckBox.IsChecked = false;
                }
            }
            while (ClosedPopup == false)
            {
                await Task.Delay(10);
            }
        }

        string PassError = "";
        private async Task AutoClosePopup(int Seconds)
        {
            for (int i = Seconds; i > 0; i--)
            {
                CloseButton.Content = i.ToString();
                await Task.Delay(1000);
                if (AbortCountDown == true)
                {
                    AbortCountDown = false;
                    break;
                }
            }
            CloseButton.Content = "0";
            await ClosePopup();
        }
        bool AbortCountDown = false;
        private async Task ClosePopup()
        {
            SoundPlayer h = new SoundPlayer(Properties.Resources.Click);
            h.Play();
            AbortCountDown = true;
            Topmost = true;
            CloseButton.IsEnabled = false;
            CloseButton.Content = "Wait..";
            await ShowSnackBar("Connecting to internet, please wait...");
            ErrorLabel.Content = "Waiting for internet connection";
            ReasonLabel.Content = "Please wait for an internet connection before this popup will\nclose, reaction time is near instant as soon as internet is restored.";
            BackgroundWorker ContactTimer = new BackgroundWorker();
            ContactTimer.WorkerSupportsCancellation = true;
            bool FireContact = false;
            bool FiredContact = false;
            ContactTimer.DoWork += async (sender, args) =>
            {
                await Task.Delay(15000);
                FireContact = true;
            };
            ContactTimer.RunWorkerAsync();
            while (await Functions.CheckInternet() == false)
            {
                if (FireContact == true && FiredContact == false)
                {
                    FiredContact = true;
                    await Functions.PleaseContactSound();
                }
                await Task.Delay(10);
            }
            await ShowSnackBar("Internet has been restored");
            await Task.Delay(1000);
            CloseButton.Content = "Close";
            CloseButton.IsEnabled = true;
            Visibility = Visibility.Hidden;
            await OtherFunctions.ShowNotification("Internet connection has been restored", "You can now use the internet");
            SoundPlayer Player = new SoundPlayer(Properties.Resources.InternetRestored_SP);
            Player.Play();
            ClosedPopup = true;
        }

        private async Task DoSpeedTest(bool NetGeared_Status)
        {
            SpeedTest Speed = new SpeedTest();
            Speed.Error = PassError;
            Speed.NetGearedSet = NetGeared_Status;
            await Speed.Start();
            SpeedTestGrid.Visibility = Visibility.Visible;
            MainPopupGrid.Visibility = Visibility.Hidden;
            Visibility = Visibility.Visible;
            Topmost = true;
            DownloadLabel.Content = "DOWNLOAD: " + Speed.DownloadSpeed;
            UploadLabel.Content = "UPLOAD: " + Speed.UploadSpeed;
            PingLabel.Content = "PING: " + Speed.Ping;
            ISPLabel.Content = "Internet service provider: " + Speed.InternetServiceProvider;
            SpeedTestURL = Speed.ResultURL;
            if (GamingMode == true)
            {
                Visibility = Visibility.Hidden;
            }
        }

        private async Task NetGearedPopup()
        {
            await Functions.PlaySoundSync(Properties.Resources.Outage);
            await Functions.PlaySoundSync(Properties.Resources.TPLinked);
            ErrorLabel.Background = Brushes.Red;
            int TimeOut = 0;
            Topmost = true;
            Visibility = Visibility.Visible;
            ErrorLabel.Content = "ASUSED";
            ReasonLabel.Content = "ASUSED";
            ErrorLabel.Background = Brushes.Red;
            ReasonLabel.Background = Brushes.Red;
            CloseButton.IsEnabled = false;
            Topmost = true;
            while (await Functions.CheckNetGear() == false || TimeOut == 120)
            {
                await Task.Delay(1000);
                TimeOut++;
            }
            ErrorLabel.Background = Brushes.Lime;
            ReasonLabel.Background = Brushes.Lime;
            await Functions.PlaySoundSync(Properties.Resources.Restore);
            CloseButton.IsEnabled = true;
            await ShowSnackBar("Please fix Asus router");
            await PopupSound("ROUTER ASUSED", "Your router is \"ASUSED\", please ask your brother to fix it,\n or please be patient while the Asus router restarts.", Properties.Resources.InternetMayNotBeAvailable);
            ErrorLabel.Background = Brushes.Transparent;
            if (NetGearCheckBox.IsChecked == false)
            {
                OwnsNetGear = false;
            }
            await DoSpeedTest(true);
            ErrorLabel.Background = Brushes.Transparent;
            ReasonLabel.Background = Brushes.Transparent;
        }
    }
}



// Weather popups //
//bool CheckContain(string OriginalString, string StringToCheck)
//{
//    CultureInfo culture = new CultureInfo("en-US", false);
//    return culture.CompareInfo.IndexOf(OriginalString, StringToCheck, CompareOptions.IgnoreCase) >= 0;
//}
//AbortableBackgroundWorker FirstInfo = new AbortableBackgroundWorker();
//FirstInfo.DoWork += async (o, args) =>
//{
//    NewWeather = await MarkhamWeatherInfo.GetWeather();
//};
//FirstInfo.RunWorkerAsync();
//while (FirstInfo.IsBusy)
//{
//    await Task.Delay(10);
//    if (await Functions.CheckInternet() == false)
//    {
//        FirstInfo.Abort();
//        FirstInfo.Dispose();
//        break;
//    }
//}
//Console.WriteLine("Current Weather: " + NewWeather + "\nOld Weather: " + OldWeather);
//if (OldWeather != NewWeather)
//{
//    OldWeather = NewWeather;
//    Console.WriteLine("WEATHER CHANGED");
//    string WeatherCondition = OldWeather;
//    bool CheckWeatherCondition(string WeatherInfo)
//    {
//        return CheckContain(WeatherCondition,WeatherInfo);
//    }

//    if (await Functions.CheckInternet() == true)
//    {
//        Temperatures WeatherJSON = new Temperatures(); 
//        AbortableBackgroundWorker GetInfo = new AbortableBackgroundWorker();
//        GetInfo.DoWork += (o, args) =>
//        {
//            WeatherJSON = QuickType.Temperatures.FromJson(MarkhamWeatherInfo.GetWeatherFromJSON());
//        };
//        GetInfo.RunWorkerAsync();
//        while (GetInfo.IsBusy)
//        {
//            await Task.Delay(10);
//            if (await Functions.CheckInternet() == false)
//            {
//                Console.WriteLine("ABORTED");
//                GetInfo.Abort();
//                GetInfo.Dispose();
//                break;
//            }
//        }
//        string Tempurature = "";
//        foreach (Datum datum in WeatherJSON.Data)
//        {
//            Tempurature = datum.Temp.ToString();
//        }
//        OtherFunctions.ShowNotification("WEATHER CHANGED",
//            "Weather has been changed to: " + OldWeather + "\nTempurature: " + Tempurature);
//        if (CheckWeatherCondition("Thunderstorm"))
//        {
//            await PlayWeatherSound(WeatherSound.Thunderstorm);
//            await PopupSound("WEATHER CHANGED",
//                "Weather has been changed to " + NewWeather + "\nTempurature: " + Tempurature,
//                Properties.Resources.ThunderStorm);
//        }
//        else if (CheckWeatherCondition("Drizzle") || CheckWeatherCondition("Rain"))
//        {
//            await PlayWeatherSound(WeatherSound.Rain);
//            await PopupSound("WEATHER CHANGED",
//                "Weather has been changed to " + NewWeather + "\nTempurature: " + Tempurature,
//                Properties.Resources.Rain);
//        }
//        else if (CheckWeatherCondition("Snow") || CheckWeatherCondition("Sleet") ||
//                 CheckWeatherCondition("Flurries"))
//        {
//            await PlayWeatherSound(WeatherSound.Snow);
//            await PopupSound("WEATHER CHANGED",
//                "Weather has been changed to " + NewWeather + "\nTempurature: " + Tempurature,
//                Properties.Resources.Snow);
//        }
//        else if (CheckWeatherCondition("Mist") || CheckWeatherCondition("Smoke") ||
//                 CheckWeatherCondition("Haze") || CheckWeatherCondition("Fog") ||
//                 CheckWeatherCondition("Sand"))
//        {
//            await PlayWeatherSound(WeatherSound.Breezy);
//            await PopupSound("WEATHER CHANGED",
//                "Weather has been changed to " + NewWeather + "\nTempurature: " + Tempurature,
//                Properties.Resources.Cloudy);
//        }
//        else if (CheckWeatherCondition("Clear Sky"))
//        {
//            await PlayWeatherSound(WeatherSound.Sunny);
//            await PopupSound("WEATHER CHANGED",
//                "Weather has been changed to " + NewWeather + "\nTempurature: " + Tempurature,
//                Properties.Resources.ClearSky);
//        }
//        else if (CheckWeatherCondition("Cloud"))
//        {
//            await PlayWeatherSound(WeatherSound.FogCloudy);
//            await PopupSound("WEATHER CHANGED",
//                "Weather has been changed to " + NewWeather + "\nTempurature: " + Tempurature,
//                Properties.Resources.Cloudy);
//        }
//        else if (CheckWeatherCondition("Unknown Precipitation"))
//        {
//            await PlayWeatherSound(WeatherSound.Rain);
//            await PopupSound("WEATHER CHANGED",
//                "Weather has been changed to " + NewWeather + "\nTempurature: " + Tempurature,
//                Properties.Resources.Rain);
//        }
//    }
//}