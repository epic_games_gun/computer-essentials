﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MailKit;
using MailKit.Net.Imap;

namespace Main_Tools_Remastered.Properties
{
    public class MainFunctions
    {
        public MainFunctions()
        {
 
        }
        public async Task HoldFile()
        {
            while (File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\HoldMonitoring.txt"))
            {
                await Task.Delay(10);
            }
        }
        public async Task PleaseContactSound()
        {
            string URL = "";
            string ISP = "";
            foreach (var readLine in File.ReadLines(Environment.GetEnvironmentVariable("TEMP") +
                                                    "\\SpeedTest\\Log.txt"))
            {
                if (readLine.Contains("Result URL"))
                {
                    URL = readLine.Replace("Result URL:", "");
                    URL = URL.Replace(" ", "");
                }

                if (readLine.Contains("ISP"))
                {
                    ISP = readLine.Replace("       ISP: ", "");
                    ISP = readLine.Replace(" ", "");
                }
            }

            await PlaySoundSync(Resources.PleaseContact);
            if (ISP.Contains("Carry") || ISP.Contains("carry"))
            {
                await PlaySoundSync(Resources.CarryTelecom);
            }
            else if (ISP.Contains("Rogers") || ISP.Contains("rogers"))
            {
                await PlaySoundSync(Resources.RogersISP);
            }
            else if (ISP.Contains("Fido") || ISP.Contains("fido"))
            {
                await PlaySoundSync(Resources.Fido);
            }
            else if (ISP.Contains("Bell") || ISP.Contains("bell"))
            {
                await PlaySoundSync(Resources.BellCanada);
            }
            else if (ISP.Contains("Virgin Mobile") || ISP.Contains("Virgin") || ISP.Contains("virgin"))
            {
                await PlaySoundSync(Resources.VirginMobile);
            }
            else if (ISP.Contains("CIK"))
            {
                await PlaySoundSync(Resources.CIK);
            }
            else if (ISP == "")
            {
                await PlaySoundSync(Resources.NotConnected);
            }
            else
            {
                await PlaySoundSync(Resources.ConnectedToVPN);
            }
            await PlaySoundSync(Resources.IfYouStillDoNotHaveInternet);
        }
        public async Task PlaySoundSync(Stream Location)
        {
            TimeSpan start = new TimeSpan(8, 0, 0); //10 o'clock
            TimeSpan end = new TimeSpan(23, 0, 0); //12 o'clock
            TimeSpan now = DateTime.Now.TimeOfDay;

            if ((now > start) && (now < end))
            {
                //match found
                SoundPlayer d = new SoundPlayer(Location);
                await Task.Factory.StartNew(() =>
                {
                    d.PlaySync();
                });
            }
            else
            {
                
            }
        }

        public async Task FireContactISP()
        {
            if(File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\SpeedTest\\Log.txt"))
            {
                string ISP = "";
                foreach (var readLine in File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\SpeedTest\\Log.txt"))
                {
                    if(readLine.Contains("ISP"))
                    {
                        ISP = readLine.Split(':')[1].Trim().Replace(" ", "");
                        break;
                    }
                }
                CultureInfo info = new CultureInfo("en-US");
                await PlaySoundSync(Resources.PleaseContact);
                if (ISP.Contains("Carry") || ISP.Contains("carry"))
                {
                    await PlaySoundSync(Resources.CarryTelecom);
                }
                else if (ISP.Contains("Rogers") || ISP.Contains("rogers"))
                {
                    await PlaySoundSync(Resources.RogersISP);
                }
                else if (ISP.Contains("Fido") || ISP.Contains("fido"))
                {
                    await PlaySoundSync(Resources.Fido);
                }
                else if (ISP.Contains("Bell") || ISP.Contains("bell"))
                {
                    await PlaySoundSync(Resources.BellCanada);
                }
                else if (ISP.Contains("Virgin Mobile") || ISP.Contains("Virgin") || ISP.Contains("virgin"))
                {
                    await PlaySoundSync(Resources.VirginMobile);
                }
                else if (ISP.Contains("CIK") || info.CompareInfo.IndexOf(ISP,"CIK",CompareOptions.IgnoreCase) >= 0)
                {
                    await PlaySoundSync(Resources.CIK);
                }
                else if (ISP == "")
                {
                    await PlaySoundSync(Resources.NewNotConnectedToInternet);
                }
                else
                {
                    await PlaySoundSync(Resources.NewConnectedToVPN);
                }
                await PlaySoundSync(Resources.DontHaveInternet);
            }
        }

        public async Task<bool> PureCheckInternet()
        {
            bool Return = false;
            string CheckString = "";

            BackgroundWorker Checker = new BackgroundWorker();
            Checker.DoWork += (sender, args) =>
            {
                try
                {
                    CheckString = new WebClient().DownloadString("http://www.msftncsi.com/ncsi.txt");
                }
                catch
                {
                    Return = false;
                }
            };
            Checker.RunWorkerAsync();
            while (Checker.IsBusy)
            {
                await Task.Delay(10);
            }
            Checker.Dispose();
            if (CheckString == "Microsoft NCSI")
            {
                Return = true;
            }
            else
            {
                Return = false;
            }

            return Return;
        }
        public async Task<bool> CheckInternet()
        {
            bool Return = false;
            string CheckString = "";
            BackgroundWorker Checker = new BackgroundWorker();
            Checker.DoWork += (sender, args) =>
            {
                try
                {
                    CheckString = new WebClient().DownloadString("http://www.msftncsi.com/ncsi.txt");
                }
                catch
                {
                    Return = false;
                }
            };
            Checker.RunWorkerAsync();
            while (Checker.IsBusy)
            {
                await Task.Delay(10);
            }
            Checker.Dispose();
            if (CheckString == "Microsoft NCSI")
            {
                Return = true;
            }
            else
            {
                Return = false;
            }
            return Return;
        }

        bool SetOnceNonRestrict = false;
        bool NonRestricted = false;

        public async Task<bool> CheckNetGear()
        {
            bool Return = false;
            string CheckNetGear = "";
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    NonRestricted = new WebClient().DownloadString("http://192.168.50.1/Main_Login.asp").Contains("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
                }
                catch (Exception)
                {
                    NonRestricted = false;
                }
            });
            BackgroundWorker Checker = new BackgroundWorker();
            Checker.DoWork += (sender, args) =>
            {
                try
                {
                    CheckNetGear = new WebClient().DownloadString("http://192.168.50.1/Main_Login.asp");
                }
                catch (Exception e)
                {
                    CheckNetGear = "";
                }
            };
            Checker.RunWorkerAsync();
            while (Checker.IsBusy)
            {
                await Task.Delay(10);
            }
            Checker.Dispose();
            if(CheckNetGear.Contains("http://192.168.50.1:80/blocking.asp?c") || CheckNetGear.Contains("<html xmlns=\"http://www.w3.org/1999/xhtml\">"))
            {
                Return = true;
            }
            else
            {
                Return = false;
            }
            return Return;
        }

        public async Task<string> CheckIP()
        {
            string Return = ""; 
            BackgroundWorker Checker = new BackgroundWorker();
            Checker.DoWork += (sender, args) =>
            {
                try
                {
                    Return = new WebClient().DownloadString("http://icanhazip.com");
                }
                catch
                {

                }
            };
            Checker.RunWorkerAsync();
            while (Checker.IsBusy)
            {
                await Task.Delay(10);
            }
            Checker.Dispose();
            return Return;
        }
    }
}
