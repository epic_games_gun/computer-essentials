﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROBLOX_Booster
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Finding Roblox Versions");
            if(Directory.Exists(@"C:\Program Files (x86)\Roblox\Versions"))
            {
                Console.WriteLine("Found");
                DirectoryInfo VersionsDirectory = new DirectoryInfo("C:\\Program Files (x86)\\Roblox\\Versions");
                foreach (DirectoryInfo directoryInfo in VersionsDirectory.GetDirectories())
                {
                    if (directoryInfo.FullName.Contains("version-"))
                    {
                        Console.WriteLine("Deleting textures from one folder...");
                        if (Directory.Exists(directoryInfo.FullName + "\\PlatformContent\\pc\\textures"))
                        {
                            DeleteDirectory(directoryInfo.FullName + "\\PlatformContent\\pc\\textures");
                        }
                        Directory.CreateDirectory(directoryInfo.FullName + "\\PlatformContent\\pc\\textures");
                    }
                }
                Console.WriteLine("Roblox is now boosted!");
            }
        }

        public static void DeleteDirectory(string path)
        {
            foreach (string directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }
    }
}
