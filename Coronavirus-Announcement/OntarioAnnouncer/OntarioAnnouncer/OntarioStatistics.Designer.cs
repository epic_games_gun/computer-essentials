﻿
namespace OntarioAnnouncer
{
    partial class OntarioStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.OntarioVaccineStatistics = new System.Windows.Forms.RichTextBox();
            this.RegionsStatistics = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ontario Vaccine Statistics:";
            // 
            // OntarioVaccineStatistics
            // 
            this.OntarioVaccineStatistics.Location = new System.Drawing.Point(11, 41);
            this.OntarioVaccineStatistics.Name = "OntarioVaccineStatistics";
            this.OntarioVaccineStatistics.Size = new System.Drawing.Size(540, 168);
            this.OntarioVaccineStatistics.TabIndex = 1;
            this.OntarioVaccineStatistics.Text = "";
            // 
            // RegionsStatistics
            // 
            this.RegionsStatistics.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.RegionsStatistics.Location = new System.Drawing.Point(13, 251);
            this.RegionsStatistics.Name = "RegionsStatistics";
            this.RegionsStatistics.Size = new System.Drawing.Size(670, 385);
            this.RegionsStatistics.TabIndex = 2;
            this.RegionsStatistics.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(12, 223);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Regions:";
            // 
            // OntarioStatistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(1269, 641);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RegionsStatistics);
            this.Controls.Add(this.OntarioVaccineStatistics);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "OntarioStatistics";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ontario Statistics";
            this.Load += new System.EventHandler(this.OntarioStatistics_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox OntarioVaccineStatistics;
        private System.Windows.Forms.RichTextBox RegionsStatistics;
        private System.Windows.Forms.Label label2;
    }
}