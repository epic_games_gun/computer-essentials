﻿// <auto-generated />
//
// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using OntarioVaccineAPI;
//
//    var ontarioVaccine = OntarioVaccine.FromJson(jsonString);

namespace OntarioVaccineAPI
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class OntarioVaccine
    {
        [JsonProperty("first_dose_eligibility")]
        public string FirstDoseEligibility { get; set; }

        [JsonProperty("vaccine_brand")]
        public string VaccineBrand { get; set; }

        [JsonProperty("second_dose_date")]
        public DateTimeOffset SecondDoseDate { get; set; }
    }

    public partial class OntarioVaccine
    {
        public static OntarioVaccine FromJson(string json) => JsonConvert.DeserializeObject<OntarioVaccine>(json, OntarioVaccineAPI.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this OntarioVaccine self) => JsonConvert.SerializeObject(self, OntarioVaccineAPI.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}