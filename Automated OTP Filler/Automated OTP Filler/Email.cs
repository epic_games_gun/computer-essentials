﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;
using MimeKit;
using UsefulTools;

namespace Automated_OTP_Filler
{
    public class Email
    {
        public string Email_ = "";
        public string Password_ = "";

        public MailKit.Net.Imap.ImapClient imapclient = new ImapClient();
        public Email(string Email, string Password)
        {
            SpeechSynthesizer speech = new SpeechSynthesizer();
            Email_ = Email;
            Password_ = Password;
            imapclient.Disconnected += async (sender, args) =>
            {
                Console.WriteLine("Client Disconnected");
                speech.Speak("Client was disconnected");
                await Internet.CheckInternet();
                Login();
            };
            imapclient.Connected += (sender, args) =>
            {
                Console.WriteLine("Client Connected");
                speech.Speak("Client is connected");
            };
            Login();
        }


        private void Login()
        {
            imapclient.Connect("imap-mail.outlook.com", 993,true);
            imapclient.Authenticate(Email_,Password_);
            imapclient.Inbox.Open(FolderAccess.ReadWrite);
        }


        public List<MimeMessage> GetEmails()
        {
            List<MimeMessage> Return = new List<MimeMessage>();
            imapclient.Inbox.Open(FolderAccess.ReadWrite);
            var uid = imapclient.Inbox.Search(SearchQuery.All);
            for (int i = 0; i < uid.Count; i++)
            {
                var message = imapclient.Inbox.GetMessage(uid.Count - i - 1);
                Return.Add(message);
            }
            return Return;
        }

        public void MoveEmailsToFinished()
        {
            var uid = imapclient.Inbox.Search(SearchQuery.All);
            var FinishedFolder = imapclient.Inbox.Create("Finished Orders" + new Random().Next(111, 999).ToString(), true);
            FinishedFolder.Open(FolderAccess.ReadWrite);
            imapclient.Inbox.Open(FolderAccess.ReadWrite);
            for (int i = 0; i < uid.Count; i++)
            {
                var message = imapclient.Inbox.GetMessage(uid.Count - i - 1);
                if (!message.Subject.Contains("Welcome to your new Outlook.com account"))
                    imapclient.Inbox.MoveTo(uid.Count - i - 1, FinishedFolder);
            }
        }

        public void DeleteMessage(string Contain)
        {
            var uid = imapclient.Inbox.Search(SearchQuery.All);
            var FinishedFolder = imapclient.Inbox.Create("Finished Orders" + new Random().Next(111, 999).ToString(), true);
            FinishedFolder.Open(FolderAccess.ReadWrite);
            imapclient.Inbox.Open(FolderAccess.ReadWrite);
            for (int i = 0; i < uid.Count; i++)
            {
                var message = imapclient.Inbox.GetMessage(uid.Count - i - 1);
                if (message.Subject.Contains(Contain))
                    imapclient.Inbox.MoveTo(uid.Count - i - 1, FinishedFolder);
            }
            FinishedFolder.Delete();
            foreach (IMailFolder mailFolder in imapclient.GetFolders(new FolderNamespace('/',"Inbox")))
            {
                mailFolder.Delete();
            }
        }
        public void MoveMessageToFolder(string Subject, IMailFolder folder)
        {
            imapclient.Inbox.Open(FolderAccess.ReadWrite);
            folder.Open(FolderAccess.ReadWrite);
            var uid = imapclient.Inbox.Search(SearchQuery.All);
            for (int i = 0; i < uid.Count; i++)
            {
                var message = imapclient.Inbox.GetMessage(uid.Count - i - 1);
                if (message.Subject.Contains(Subject))
                {
                    imapclient.Inbox.MoveTo(uid.Count - i - 1,folder);
                }
            }
        }
        public void CreateFolder(string name)
        {
            var Folder = imapclient.Inbox.Create(name,true);
            Folder.Open(FolderAccess.ReadWrite);
        }
        public IMailFolder FindFolder(string Folder)
        {
            return imapclient.GetFolder(Folder);
        }

        public void OpenFolder(IMailFolder folder)
        {
            folder.Open(FolderAccess.ReadWrite);
        }

        public void DeleteFolder(IMailFolder folder)
        {
            folder.Delete();
        }
    }
}
